### Sample note 
---

1. **List item 1** 
	- Point 1
	- Example:  
	![Alt text](img/sample-imgs/sample-imgs-1.png)
	- Using **PNG** instead of ~~png~~
	- ![Alt text](img/sample-imgs/sample-imgs-1.PNG)
2. **List item 2**
	- Point 2
	- Example
	![Alt text](img/sample-imgs/sample-imgs-2.png)
	- Using **PNG** instead of ~~png~~
	- ![Alt text](img/sample-imgs/sample-imgs-2.PNG)


----

### An attempt with `./img/` and `.img/`


1. **List item 1** 
	- Point 1
	- Example:  
	![Alt text](./img/sample-imgs/sample-imgs-1.png)
    - Using PNG instead of ~~png~~
    - ![Alt text](./img/sample-imgs/sample-imgs-1.PNG)
2. **List item 2**
	- Point 2
	- Example
	![Alt text](./img/sample-imgs/sample-imgs-2.png)
	- Using PNG instead of ~~png~~
	- ![Alt text](./img/sample-imgs/sample-imgs-2.PNG) 

